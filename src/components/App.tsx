import React from 'react';
import '../App.css';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import OrdersDataGqlProvider, { OrderData } from './demo/providers/OrdersDataGqlProvider'
import KanbanBoard, {IssueView} from '../modules/exhibitor/presenters/kanban/KanbanBoard';
import KanbanColumn from '../modules/exhibitor/presenters/kanban/KanbanColumn';
import DataGrid from '../modules/exhibitor/presenters/grid/DataGrid';
import GridColumn from '../modules/exhibitor/presenters/grid/GridColumn';
import GridFilter from '../modules/exhibitor/presenters/grid/GridFilter';

function App() {

    const client = new ApolloClient({
        uri: 'http://localhost',
    });

    const KanbanTask: IssueView = (props: {issue: OrderData}) => {
        const { issue } = props
        return (
            <div style={{backgroundColor: '#ccc', borderRadius: '5px', margin: '5px', padding: '3px', color: '#111'}}>
                {issue.name}
            </div>
        )
    }

    const isNew = (order: OrderData) => {
        return parseInt(order.id)%2===0
    }

    const isInProgress = (order: OrderData) => {
        return parseInt(order.id) > 17
    }

    const isDone = (order: OrderData) => {
        return parseInt(order.id) > 7
    }

    return (
        <div className="App">

            <ApolloProvider client={client}>

                <div style={{ float: 'left', width: '50%' }}>

                    <DataGrid provider={new OrdersDataGqlProvider(client)}>

                        <GridFilter id="search">asd
                            <input type="text" value="asd"/>
                        </GridFilter>

                        <GridColumn id="id" name="ID"/>
                        <GridColumn id="name">
                            { (order: OrderData) => { return order.name?.toUpperCase() ?? '' } }
                        </GridColumn>
                        <GridColumn id="name2" dependencies={['name']} hidden={true} />
                        <GridColumn id="number1" dependencies={['test.number']} name="Numer1" hidden={true} />
                        <GridColumn id="number2" dependencies={['test.number']} name="Numer2" hidden={true}>
                            { (order:OrderData) => order.test?.number }
                        </GridColumn>

                    </DataGrid>

                </div>

                <div style={{ float: 'left', width: '50%' }}>
                    <KanbanBoard provider={new OrdersDataGqlProvider(client)}>
                        <KanbanColumn name="Nowe" condition={isNew} dependencies={['id', 'name']}>
                            { (issue:OrderData) => <KanbanTask key={issue.id} issue={ issue }/> }
                        </KanbanColumn>
                        <KanbanColumn name="W toku" condition={isInProgress} />
                        <KanbanColumn name="Gotowe" condition={isDone} />
                    </KanbanBoard>
                </div>

            </ApolloProvider>

        </div>
    );
}

export default App;

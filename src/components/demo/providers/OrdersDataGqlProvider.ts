import { DataInterface, Field, QueryResult } from '../../../modules/exhibitor/providers/types';
import { GqlProvider } from '../../../modules/exhibitor/providers/GqlProvider';
import { GqlBuilder } from 'gql-builder';

export type OrderData = {
    id: string
    name?: string
    test?: {
        number: string
    }
}

export interface OrderQueryResult extends QueryResult {
    data: OrderData[]
}

const searchFilter = () => {
    // value: ''
    // onFilter: () => {
        // usedFilters['search'] = 'String'
        // this.addVariable('search', 'asd123')
    // }
}

class OrdersDataGqlProvider extends GqlProvider implements DataInterface {

    load = () => {
        this.executeQuery()
    }

    protected applyFilters = () => {
        // this.query.
        // applyFilters
    }

    mapResultToData = (result: any) => {
        return { data: result.data.Orders.All ?? [] }
    }

    availableFilters = () => {
        return [
            searchFilter
        ];
        // return {
        //     // TODO: build dynamically by api schema
        //     search: {
        //         type: 'text'
        //     },
        //     choose: {
        //         type: 'multiselect',
        //         options: [
        //             {value: 'ASC', name: 'Test multiselect 1'},
        //             {value: 'DESC', name: 'Test multiselect 2'}
        //         ]
        //     }
        // }
    }

    usedFilters = () => {
        // let usedFilters:any = {}

        // usedFilters['search'] = 'String'
        // this.addVariable('search', 'asd123')
    }

    protected getQueryString = () => {
        let ordersQuery = {
            name: 'Orders',
            fields: [{
                name: 'All',
                filters: {
                },
                fields: []
            }]
        } as any;

        if ('first' in this.query.variables) {
            ordersQuery.fields[0].filters['first'] = 'Int'
        }

        if ('skip' in this.query.variables) {
            ordersQuery.fields[0].filters['skip'] = 'Int'
        }
        
        // filters: this.usedFilters()

        let fieldsCursor = ordersQuery.fields[0].fields;

        this.buildQueryFields(this.query.fields, fieldsCursor);

        const qb = new GqlBuilder();
        qb.addSibling(ordersQuery);

        return qb.toQueryString();
    }
}

export default OrdersDataGqlProvider
import React, { useState, useEffect } from 'react';
import GridColumn from './GridColumn';
import { DataInterface, QueryResult } from '../../providers/types';
import objectPath from 'object-path'
import GridFilter from './GridFilter';

type Props = {
    provider: DataInterface
    children: any[]
}

const DataGrid = (props: Props) => {

    const { provider } = props;

    const allColumns = () => {
        return props.children ? props.children.filter(el => el.type === GridColumn).map(column => ({
            id: column.props.id,
            name: column.props.name ?? column.props.id,
            visible: !column.props.hidden,
            virtual: column.props.virtual,
            val: column.props.val,
            dependencies: column.props.dependencies,
            children: column.props.children
        })) : [];
    }

    const [gridData, setGridData] = useState({data: []} as QueryResult);
    const [gridColumns, setGridColumns] = useState(allColumns())
    const [limit, setLimit] = useState(10)
    const [page, setPage] = useState(1)

    const toogleVisibility = (column:any) => {
        setGridColumns(gridColumns.map(col => {
            if (col.id === column.id) {
                col.visible = !col.visible
            }

            return col
        }))
    }

    let header = gridColumns.filter(col => col.visible).map((col, i) => {
        return <th key={ i }>{ col.name }</th>
    });

    const queryFields = () => {
        let fields = []

        for (let col of gridColumns) {
            if (col.visible) {
                if (!col.dependencies) {
                    fields.push({id: col.id})
                } else {
                    for (let dependency of col.dependencies) {
                        fields.push({id: dependency})
                    }
                }
            }
        }
        
        return fields;
    }

    useEffect(() => {
        console.log('useEffect')
        provider.only(queryFields());
        provider.first(limit);
        provider.skip((page-1)*limit);
        provider.onResult( (result:any) => {
            setGridData(provider.mapResultToData(result));
        })
        provider.load();
    }, [limit, page, gridColumns]); // TODO: set correct dependencies

    let data = () => {

        console.log('data reload')
        if (gridData.data.length === 0) {
            return <tr><td>loading</td></tr>
        }

        let rowKey = 0;

        return gridData.data.map(el => {
            let cells = []
            let cellKey = 0;
            for (let c of gridColumns.filter(col => col.visible)) {        
                let val

                if(c.children) {
                    val = c.children(el)
                } else {
                    val = objectPath.get(el, (c.dependencies && c.dependencies[0]) ? c.dependencies[0] : c.id);
                }

                cells.push(<td key={rowKey+'.'+cellKey++}>{ val }</td>) 
            }

            return <tr key={ rowKey++ }>{ cells }</tr>
        })
    }

    let filters = () => {
        return props.children && props.children.filter(el => el.type === GridFilter).map(filter => {
            // console.log('asd', filter);
            return filter.props?.children ?? '--';
        });
    }

    let pagination = () => {
        let links = [];

        for (let i=1; i < 10; i++) {
            links.push(<a key={i} className={ i===page ? 'active' : '' } href="#" onClick={() => {
                setPage(i)
            }}>{ i } </a>) 
        }
        
        return (
            <div className="pagination">
                { links }
                <select onChange={(val) => {
                    setLimit(parseInt(val.target.value))
                }}>
                    <option value="10">10</option>
                    <option value="20">20</option>
                </select>
            </div>
        )
    }

    return (
        <>
            { filters() }
            {
                gridColumns.map((col, i) => {
                    return (
                        <p key={ i }>
                            <input type="checkbox" defaultChecked={col.visible} onClick={ () => {
                                toogleVisibility(col)
                            }}/> { col.name }
                        </p>
                    );
                })
            }

            <table cellPadding="2" cellSpacing="2" className="react-grid">
                <thead>
                    <tr>
                        { header }
                    </tr>
                </thead>
                <tbody>                
                    { data() }
                </tbody>
            </table>

            { pagination() }
        </>
    );
}


export default DataGrid;
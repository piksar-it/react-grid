import React, { useState, useEffect } from 'react';
import { DataInterface, QueryResult } from '../../providers/types';
import KanbanColumn from './KanbanColumn';

type Props = {
    provider: DataInterface
    children: any[]
}

export type IssueView = (issue: any) => JSX.Element;

const KanbanBoard = (props: Props) => {

    const { provider } = props;

    const allColumns = () => {
        return props.children ? props.children.filter(el => el.type === KanbanColumn).map(column => ({
            name: column.props.name,
            condition: column.props.condition,
            children: column.props.children,
            dependencies: column.props.dependencies
        })) : [];
    }

    const queryFields = () => {
        let fields = []

        for (let col of allColumns()) {
            if (col.dependencies) {
                for (let dependency of col.dependencies) {
                    fields.push({id: dependency})
                }
            }
        }

        return fields;
    }
    
    const [kanbanData, setKanbanData] = useState({data: []} as QueryResult);

    useEffect(() => {
        provider.only(queryFields());
        provider.first(20);
        provider.onResult( (result:any) => {
            setKanbanData(provider.mapResultToData(result));
        })
        provider.load();
    }, [true]); // TODO: set correct dependencies

    let data = (condition: (el: any) => boolean, childrenElement?: IssueView) => {
        if (kanbanData.data.length === 0) {
            return 'loading'
        }

        return kanbanData.data.filter((el:any) => condition(el)).map((el:any, i: any) => {

            if (childrenElement) {
                return childrenElement(el);
            }

            return (
                <div key={i} style={{ margin: '3px', background: '#777' }}>
                    { JSON.stringify(el) }
                </div>
            )
        })
    }

    return (
        <>
            {
                allColumns().map((col, i) => {
                    return (<div key={i} style={{float: 'left', borderRight: '1px solid #666', width: (100/allColumns().length-1)+'%'}}>
                        <h3>{ col.name }</h3>
                        <div>
                            { data(col.condition, col.children) }
                        </div>
                    </div>)
                })
            }
        </>
    );
}

export default KanbanBoard;
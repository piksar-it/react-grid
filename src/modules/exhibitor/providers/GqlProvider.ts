import { Field, Filter, Sorter } from "./types";
import { gql, } from "apollo-boost";
import ApolloClient from 'apollo-boost';

export abstract class GqlProvider {

    protected client: ApolloClient<any>

    constructor(client: ApolloClient<any>) {
        this.client = client
    }

    protected query: {
        variables: any
        fields: Field[]
        onResult: (result: any) => void
    } = {
        variables: [],
        fields: [],
            onResult: (result) => { console.log('result', result) }
        }

    protected variables: any = {}

    protected abstract getQueryString: () => string

    addVariable = (key: string, val: any) => {
        this.query.variables[key] = val
    }

    only = (fields: Field[]) => {
        this.query.fields = fields;
    }

    first = (first: number) => {
        this.addVariable('first', first)
    }

    skip = (skip: number) => {
        this.addVariable('skip', skip)
    }

    onResult = (onResult: any) => {
        this.query.onResult = onResult
    }

    restrictions = () => { return [] }
    totaility = () => 100
    accessibility = () => []
    sorters = () => []

    restrict = (filters: Filter[]) => { }
    sortBy = (sorters: Sorter[]) => { }

    protected buildQueryFields = (fields: Field[], fieldsCursor: any) => {
        for (var col in fields) {
            let symbol = fields[col].id.split(".")
            let cursor = fieldsCursor;

            for (let el of symbol) {
                if (!cursor.find((element: { name: string }) => element.name === el)) {
                    cursor.push({
                        name: el,
                        fields: []
                    })
                }

                cursor = cursor[cursor.findIndex((element: { name: string }) => element.name === el)].fields
            }
        }
    }

    protected executeQuery = () => {

        console.log('execute graphQl query', this.getQueryString())

        this.client.query({
            query: gql(this.getQueryString()),
            variables: this.query.variables
        })
            .then(result => {
                this.query.onResult(result) 
            });
    }
}
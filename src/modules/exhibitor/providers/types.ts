export interface QueryResult {
    data: any[]
    count?: number
}

export type Field = {
    id: string
    name?: string
}

enum FilterType {
    Text,
    Select,
    Multiselect
}

export type Filter = {
    symbol: string
    type: FilterType
    // todo...
}
export type Sorter = {
    symbol: string
    // todo...
}

export interface DataInterface {
    load: () => void
    restrictions: () => Filter[]
    totaility: () => number
    accessibility: () => Field[]
    sorters: () => Sorter[]

    only: (fields: Field[]) => void
    first: (first: number) => void
    skip: (skip: number) => void
    restrict: (filters: Filter[]) => void
    sortBy: (sorters: Sorter[]) => void
    onResult: (result: any) => void

    mapResultToData: (result: any) => any
}